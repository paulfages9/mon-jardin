#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2017 Benoît Boudaud <https://ordinosor.wordpress.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from mon_jardin.accueil import Home
import os

dir_ = os.getcwd()
with open(dir_ + "/jardin.desktop", 'r') as file_:
    readfile = file_.readlines()
    for i in readfile:
        if i.startswith("Exec"):
            readfile[readfile.index(i)] = "Exec=./jardin.py\n"
        if i.startswith("Icon"):
            readfile[readfile.index(i)] = "Icon=" + dir_ + "/images/mon_jardin.png\n"
with open(dir_ + "/jardin.desktop", 'w') as file_:
    for i in readfile:
        file_.write(i)

home = Home()
home.home()