Copyright © 2017 Benoît Boudaud <https://ordinosor.wordpress.com> This program is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details. You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>

## 1 - Présentation

*Mon jardin* est une application destinée aux jardiniers amateurs. Elle comprend:
* un gestionnaire de légumes,
* un gestionnaire de fruits,
* une photothèque,
* un calendrier couplé à un agenda,
* un logiciel de type « post-it ».

## 2 - Lancer l'application pour la première fois (distributions Linux)

* Vidéo explicative: https://youtu.be/1pW7V17dJQo
* Il faut télécharger le fichier compressé contenant le dossier *mon_jardin* et le fichier
*jardin.py*.
* Puis, il faut décompresser le fichier et placer *mon_jardin* et *jardin.py* dans votre répertoire
courant (par exemple, chez moi, c'est *"/home/benoit"*).
* Ensuite, il faut rendre *jardin.py* exécutable. C'est ce fichier qui lance l'application.
* Enfin, il faut entrer dans un terminal la commande suivante, par exemple chez moi :
*/home/benoit/jardin.py*. Bien sûr, vous remplacez */home/benoit* par votre répertoire courant.
* Dernière chose, assurez-vous que *python3-tk* et *python3-imaging-tk* (ou *python3-pil.imagetk*) soient bien installés sur votre distribution Linux.

## 3 - Utiliser le lanceur et créer un raccourci du lanceur sur le bureau

* Après avoir lancé l'application pour la première fois en suivant les instructions du paragraphe n°2,
il vous sera alors possible d'utiliser le lanceur et créer un raccourci sur votre bureau. Ce lanceur se trouve dans le dossier *mon_jardin* et est illustré par une photo
montrant une caisse de légumes sur une chaise bleue. 
Il suffit de cliquer dessus pour lancer l'application.

## 4 - Mises à jour

J’apporte régulièrement des modifications au code donc pour mettre à jour l’application et ne pas
détruire vos données déjà enregistrées, merci de télécharger ou copier-coller uniquement les
fichiers qui se terminent par *.py*.
* *jardin.py* (fichier qui lance l’application)
* *accueil.py* (page d’accueil et page donnant accès aux différentes applications)
* *species.py* (page qui lance le gestionnaire de légumes et le gestionnaire de fruits)
* *phototheque.py* (fichier qui lance la photothèque)
* *module_notes.py* (fichier qui lance le logiciel de type post-it)
* *calendrier.py* (fichier qui lance le calendrier)
* *agenda.py* (fichier qui lance l’agenda)